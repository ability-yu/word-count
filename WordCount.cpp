#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* a[])
{
	int count = 0;
	char c;
    FILE* f = fopen(a[2], "r");
    if (f == NULL)//若打开的文件不存在或为空，则打印以下语句，并直接返回0结束程序。
    {
        printf("无可读取文本文件\n");
        return 0;
    }
    if (a[1][1] == 'w') {//若输入的控制参数是w时，则统计 单词个数 。
        while ((c = fgetc(f)) != EOF) {//当文本文件中的字符不为空，则执行循环。
            if (c == ' ' || c == ',') {//空格或逗号隔开的时候，单词数加一 
                count++;
            }
        }
        printf("单词个数：%d\n", count + 1);
    }
    else if (a[1][1] == 'c') {
        while ((c = fgetc(f)) != EOF) {//当文本文件中的字符不为空，则执行循环。
            count++;//循环一次字符数加一 
        }
        printf("字符个数：%d\n", count);
    }
    fclose(f);
	system("pause");
    return 0;
}
